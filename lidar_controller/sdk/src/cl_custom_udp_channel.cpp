/*
 * Slamtec LIDAR SDK
 *
 *  Copyright (c) 2014 - 2020 Shanghai Slamtec Co., Ltd.
 *  http://www.slamtec.com
 *
 */
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "sdkcommon.h"
#include "sl_lidar_driver.h"
#include "hal/abs_rxtx.h"
#include "hal/thread.h"
#include "hal/types.h"
#include "hal/locker.h"
#include <thread>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>
#include <unistd.h>
#include <stdexcept>

#define MAX_BUFFER_SIZE 1000000

namespace sl
{
    class CustomUdpChannel : public IChannel
    {

    public:
        struct sockaddr_in cliaddr, routaddr, servaddr;
        int sockfd;
        const char *_ip;
        int _port;
        rp::hal::Locker lock;
        std::vector<uint8_t> incomeBuffer;


    public:
        CustomUdpChannel(const char *ip, int port)
        {
            _ip = ip;
            _port = port;

            // Filling server information
            servaddr.sin_family = AF_INET;
            servaddr.sin_addr.s_addr = INADDR_ANY;
            servaddr.sin_port = htons(port);

            // Fill router info
            routaddr.sin_family = AF_INET;
            routaddr.sin_addr.s_addr = inet_addr(_ip);
            routaddr.sin_port = htons(port);
        }

        bool bind(const std::string &ip, sl_s32 port)
        {
            if (::bind(sockfd, (const struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
            {
                perror("bind failed");
                return false;
            }
            incomeBuffer.reserve(100000);
            rp::hal::Thread cacheThread = CLASS_THREAD(CustomUdpChannel, startCaching);
            if (cacheThread.getHandle() == 0)
            {
                printf("Failed creating caching thread\n");
                return false;
            }

            return true;
        }

        bool open()
        {
            if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            {
                perror("socket creation failed");
                return false;
            }
            return bind(_ip, _port);
        }

        void close()
        {
        }
        void flush()
        {
        }

        bool waitForData(size_t size, sl_u32 timeoutInMs, size_t *actualReady)
        {
            sl_u32 startTs = getms();
            sl_u32 waitTime;
            if (actualReady)
                *actualReady = size;

            while ((waitTime = getms() - startTs) <= (timeoutInMs))
            {
                if (incomeBuffer.size() >= size)
                    return true;
            }
            printf("timeout waiting for data to be ready\n");
            return false;
        }

        int write(const void *data, size_t size)
        {
            int res = sendto(sockfd, (uint8_t *)data, size, 0, (const struct sockaddr *)&routaddr, sizeof(routaddr));
            if (res < 0)
            {
                printf("Failed write to socket with errno %d \n", errno);
                return RESULT_OPERATION_FAIL;
            }
            return RESULT_OK;
        }

        bool isThereEnoughDataCached(size_t len)
        {
            return incomeBuffer.size() < len ? false : true;
        }

        sl_result startCaching()
        {
            uint8_t tmp[1024];

            while (1)
            {
                size_t ans = ::recvfrom(sockfd, &tmp, 1024, MSG_DONTWAIT, nullptr, nullptr);

                if (ans == (size_t)-1)
                {
                    continue;
                }
                try
                {
                    incomeBuffer.insert(incomeBuffer.end(), &tmp[0], &tmp[ans]);                    
                }
                catch(const std::exception& e)
                {
                    std::cerr << "temp array out of bounds with " << e.what() << '\n';
                }
                
            }
        }

        u_result recvData(void *buf, size_t len, size_t &recv_len)
        {
            while (!isThereEnoughDataCached(len))
            {
                printf("WAITING...");
            }

            lock.lock();

            try
            {
                std::copy(incomeBuffer.begin(), incomeBuffer.begin() + len, (sl_u8 *)buf);

                incomeBuffer.erase(incomeBuffer.begin(), incomeBuffer.begin() + len);
            }
            catch (int e)
            {
                printf("vector access exception. Exception number %d\n", e);
                throw std::length_error("buffer out of range");
        }

            lock.unlock();

            recv_len = len;
            return RESULT_OK;
        }

        int read(void *buffer, size_t size)
        {
            u_result ans;
            size_t lenRec = 0;

            ans = recvData(buffer, size, lenRec);

            if (ans != RESULT_OK)
            {
                printf("Failed reading the virtual buffer or rendering data\n");
            }

            return lenRec;
        }

        void clearReadCache() {}

        void setStatus(_u32 flag) {}
    };

    Result<IChannel *> createCustomUdpChannel(const char *ip, int port)
    {
        return new CustomUdpChannel(ip, port);
    }
}