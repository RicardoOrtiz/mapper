#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>
#include <cstdint>
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <vector>
#include <netinet/in.h>
#include <cstring>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include "sl_lidar_driver.h"
#include "sl_lidar.h"
#include "hal/types.h"
#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

using namespace sl;

ILidarDriver *drv;
bool isScanning = false;

pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
pcl::visualization::PCLVisualizer viewer("PCL Viewer");

void printUsage()
{
    printf("\nSlamtec S2 Lidar Controller. Use the following commands to control the lidar:\n "
           "\t-s: Start scan in normal mode\n"
           "\t-p: Stop scan\n"
           "\t-f: Force scan\n"
           "\t-r: Reset lidar\n"
           "\t-i: Get device info\n"
           "\t-h: Get device health\n"
           "\t-e: Start Express Scan (Dense)\n");
}

sl_result capture_and_display(ILidarDriver *drv)
{
    sl_lidar_response_measurement_node_hq_t nodes[8192];
    size_t count = _countof(nodes);

    sl_result op_result = drv->grabScanDataHq(nodes, count);

    cloud->width = (int)count;
    cloud->height = 1;
    cloud->resize(cloud->width * cloud->height);

    if (SL_IS_OK(op_result))
    {
        printf("node count %ld\n", count);
        for (int pos = 0; pos < (int)count; ++pos)
        {
            float theta = (nodes[pos].angle_z_q14 * 90.f) / 16384.f;
            float dist = nodes[pos].dist_mm_q2 / 4.0f;

            float x = std::sin((theta * 3.1415) / 180) * dist;
            float y = std::cos((theta * 3.1415) / 180) * dist;

            cloud->points[pos].x = x;
            cloud->points[pos].y = 0;
            cloud->points[pos].z = y;
        }
        viewer.updatePointCloud(cloud, "cloud");
        viewer.spinOnce();

        // for (int pos = 0; pos < (int)count; ++pos)
        // {
        //     printf("%s theta: %03.2f Dist: %08.2f \n",
        //            (nodes[pos].flag & SL_LIDAR_RESP_HQ_FLAG_SYNCBIT) ? "S " : "  ",
        //            (nodes[pos].angle_z_q14 * 90.f) / 16384.f,
        //            nodes[pos].dist_mm_q2 / 4.0f);
        // }
    }
    return true;
}

void getHealth()
{
    sl_lidar_response_device_health_t health;
    sl_result res = drv->getHealth(health);

    if (res != RESULT_OK)
    {
        printf("Failed getting lidar's health\n");
        return;
    }
    printf("Lidar's health status: %d\n", health.status);
    return;
}
class Controller
{
    std::atomic<bool> running;
    std::thread incomeDataThread;
    std::thread inputThread;

    static void doSomethingWithInput(const char &input)
    {

        switch (input)
        {
        case 'e':
            std::cout << "Starting Express Scan (Dense)..." << std::endl;
            isScanning = true;
            drv->startScanExpress(0, 1);
            break;
        case 's':
            std::cout << "Starting Normal Scan ..." << std::endl;
            isScanning = true;
            drv->startScan(0, 0);
            break;
        case 'p':
            std::cout << "Stopping lidar's motor ..." << std::endl;
            drv->stop();
            isScanning = false;
            break;
        case 'f':
            std::cout << "Starting force scan ..." << std::endl;
            isScanning = true;
            drv->startScan(1, 1);
            break;
        case 'r':
            std::cout << "Reset the lidar ..." << std::endl;
            drv->reset();
            break;
        case 'i':
            std::cout << "Getting lidar's info ..." << std::endl;
            break;
        case 'h':
            std::cout << "Getting lidar's health..." << std::endl;
            getHealth();
            break;
        }

        return;
    }

public:
    Controller() : running(true),
                   incomeDataThread(&Controller::readStreamData, this),
                   inputThread(&Controller::input, this)
    {
    }

    ~Controller()
    {
        inputThread.join();
        incomeDataThread.join();
    }

    void input()
    {
        while (running)
        {
            char input;
            doSomethingWithInput(std::getchar());
        }
    }

    void readStreamData()
    {
        while (running)
        {
            if (isScanning)
            {
                if (SL_IS_FAIL(capture_and_display(drv)))
                {
                    fprintf(stderr, "Error, cannot grab scan data.\n");
                }
            }
        }
    }
};

int main(int argc, const char *argv[])
{
    if (argc < 3)
        return 0;

    viewer.setBackgroundColor(0.0, 0.0, 0.0);
    viewer.addPointCloud(cloud, "cloud");

    printUsage();

    drv = *createLidarDriver();

    IChannel *channel = *createCustomUdpChannel(argv[1], strtoul(argv[2], NULL, 10));

    if (!SL_IS_OK(drv->connect(channel)))
    {
        printf("Failed connecting to CLT custom UDP channel");
        exit(EXIT_FAILURE);
    }

    static std::unique_ptr<Controller> controller = std::make_unique<Controller>();

    return 0;
}