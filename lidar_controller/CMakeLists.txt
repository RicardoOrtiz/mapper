cmake_minimum_required(VERSION 2.6 FATAL_ERROR)

project(lidar_controller)

find_package(PCL 1.2 REQUIRED)
find_package(Threads REQUIRED) # this will generate the flag for CMAKE_THREAD_LIBS_INIT
find_library(${PROJECT_SOURCE_DIR}/sdk REQUIRED)

include_directories(${PCL_INCLUDE_DIRS} (${PROJECT_SOURCE_DIR}/sdk/include) (${PROJECT_SOURCE_DIR}/sdk/src))
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (lidar_controller lidar_controller.cpp)
target_link_libraries (lidar_controller ${PCL_LIBRARIES} ${PROJECT_SOURCE_DIR}/output/Linux/Release/libsl_lidar_sdk.a ${CMAKE_THREAD_LIBS_INIT})
