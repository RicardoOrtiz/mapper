
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <netinet/in.h>
#include <cmath>

#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>

#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

#define PORT 3333
#define MAXLINE 1024
#define MAX_SCAN_NODES (4096)

#define SL_SCAN_DESCRIPTOR_HEADER_FLAG 0x81
#define SL_LIDAR_RESP_MEASUREMENT_CHECKBIT (0x1 << 0)
#define SL_LIDAR_RESP_MEASUREMENT_SYNCBIT (0x1 << 0)
#define SL_LIDAR_RESP_MEASUREMENT_INV_SYNCBIT (0x1 << 1)

#define SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT 2
#define SL_LIDAR_RESP_MEASUREMENT_ANGLE_SHIFT 1

bool ctrl_c_pressed;

// Get the S check bit
#define START_FLAG_BIT_SET(n) ((n) >> (0)) & 1

// Get the S inverse check bit
#define START_FLAG__INV_BIT_SET(n) ((n) >> (1)) & 1

// Get check bit
#define CHECK_BIT(n) ((n) >> (0)) & 1

static int sockfd;

typedef struct sl_measurement_point
{
    uint8_t quality;
    uint16_t angle_z_q14;
    uint32_t dist_mm_q2;
    uint8_t flag;
} __attribute__((packed)) sl_measurement_point;

typedef struct _sl_lidar_response_measurement_node_t
{
    uint8_t sync_quality;       // syncbit:1;syncbit_inverse:1;quality:6;
    uint16_t angle_q6_checkbit; // check_bit:1;angle_q6:15;
    uint16_t distance_q2;
} __attribute__((packed)) sl_lidar_response_measurement_node_t;

typedef struct sl_lidar_response_measurement_node_hq_t
{
    uint16_t angle_z_q14;
    uint32_t dist_mm_q2;
    uint8_t quality;
    uint8_t flag;
} __attribute__((packed)) sl_lidar_response_measurement_node_hq_t;

static void convert(const sl_lidar_response_measurement_node_t &from, sl_lidar_response_measurement_node_hq_t &to)
{
    to.angle_z_q14 = (((from.angle_q6_checkbit) >> SL_LIDAR_RESP_MEASUREMENT_ANGLE_SHIFT) << 8) / 90; // transfer to q14 Z-angle
    to.dist_mm_q2 = from.distance_q2;
    to.flag = (from.sync_quality & SL_LIDAR_RESP_MEASUREMENT_SYNCBIT);                                                      // trasfer syncbit to HQ flag field
    to.quality = (from.sync_quality >> SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT) << SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT; // remove the last two bits and then make quality from 0-63 to 0-255
}

void ctrlc(int)
{
    ctrl_c_pressed = true;
}

bool byte2Node_t(uint8_t *byteBuffer, sl_lidar_response_measurement_node_t *node)
{
    int recvPos = 0;
    uint8_t *nodeBuffer = (uint8_t *)node;

    for (size_t pos = 0; pos < 5; ++pos)
    {
        uint8_t currentByte = byteBuffer[pos];

        switch (recvPos)
        {
        case 0: // expect the sync bit and its reverse in this byte
        {
            uint8_t tmp = (currentByte >> 1);
            if ((tmp ^ currentByte) & 0x1)
            {
                // pass
            }
            else
            {
                continue;
            }
        }
        break;
        case 1: // expect the highest bit to be 1
        {
            if (currentByte & SL_LIDAR_RESP_MEASUREMENT_CHECKBIT)
            {
                // pass
            }
            else
            {
                recvPos = 0;
                continue;
            }
        }
        break;
        }
        nodeBuffer[recvPos++] = currentByte;

        if (recvPos == sizeof(sl_lidar_response_measurement_node_t))
        {
            return true;
        }
    }
    return true;
}

bool getDatagram(sl_lidar_response_measurement_node_t *nodebuffer, size_t &count)
{
    uint8_t dataBuffer[1024];
    size_t scanCounter = 0;

    while (scanCounter <= 1024)
    {
        int dataSize = recvfrom(sockfd, dataBuffer, 1024, MSG_WAITALL, nullptr, nullptr);

        if (dataSize < 0 || dataSize < 5)
        {
            printf("Failed recvfrom : %d", dataSize);
            return false;
        }

        printf("Data received from socket. Size %d\n", dataSize);

        int t = 3000;
        for (int j = 0; j < dataSize; j++)
        {
            if (START_FLAG_BIT_SET(dataBuffer[j]) && !START_FLAG__INV_BIT_SET(dataBuffer[j]) && CHECK_BIT(dataBuffer[j + 1]))
            {
                printf("Found good sequence. %02x %02x %02x %02x %02x \n", dataBuffer[j], dataBuffer[j + 1], dataBuffer[j + 2], dataBuffer[j + 3], dataBuffer[j + 4] );
                t = j;
            }
        }

        if (t == 3000)
            continue;

        for (int i = t; i <= (dataSize - t); i += 5) // TODO: make sure the dataSize is at leat 5 in size.
        {
            if ((i + 4) > dataSize)
                break;

            uint8_t recBuffer[5] = {dataBuffer[i], dataBuffer[i + 1], dataBuffer[i + 2], dataBuffer[i + 3], dataBuffer[i + 4]};

            sl_lidar_response_measurement_node_t node;

            if (!byte2Node_t(recBuffer, &node))
            {
                printf("failed creating node\n");
                continue;
            }

            sl_lidar_response_measurement_node_hq_t nodeHq;
            convert(node, nodeHq);

            printf("raw : %02x %02x %02x %02x %02x \n", dataBuffer[i], dataBuffer[i + 1], dataBuffer[i + 2], dataBuffer[i + 3], dataBuffer[i + 4]);
            printf("node_t: angle: %d dist: %d quality: %d pos : %d\n", node.angle_q6_checkbit, node.distance_q2, node.sync_quality, i);
            printf("node_hq: angle: %f dist: %f quality: %d \n\n", ((nodeHq.angle_z_q14 * 90.f) / 16384.f), (nodeHq.dist_mm_q2 / 4.0f), nodeHq.quality);

            nodebuffer[scanCounter++] = node;
        }
    }

    return true;
}

bool cacheScanData()
{
    sl_lidar_response_measurement_node_hq_t complete_scan[MAX_SCAN_NODES];
    sl_lidar_response_measurement_node_t partialBuffer[1024];
    size_t count = 1024;
    size_t scan_count = 0;

    memset(complete_scan, 0, sizeof(complete_scan));

    while (scan_count < MAX_SCAN_NODES)
    {
        bool ans = getDatagram(partialBuffer, count);

        if (!ans)
        {
            printf("failed getDatagram\n");
            return false;
        }

        for (size_t pos = 0; pos < count; ++pos)
        {
            if (++scan_count > MAX_SCAN_NODES)
                return true;

            sl_lidar_response_measurement_node_hq_t nodeHq;

            convert(partialBuffer[pos], nodeHq);

            complete_scan[scan_count++] = nodeHq; // TODO: do memset.

            // printf("angle: %f dist: %f quality: %d \n", ((nodeHq.angle_z_q14 * 90.f) / 16384.f), (nodeHq.dist_mm_q2 / 4.0f), nodeHq.quality);
            // printf("angle: %d dist: %d quality: %ld pos : %d\n", partialBuffer[pos].angle_q6_checkbit, partialBuffer[pos].distance_q2, partialBuffer[pos].sync_quality, pos);
        }
    }

    return true;
}

int main()
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");
    viewer.setBackgroundColor(0.0, 0.0, 0.0);
    viewer.addPointCloud(cloud, "cloud");

    uint8_t headerBuffer[256];
    uint8_t buffer[MAXLINE];
    struct sockaddr_in servaddr, cliaddr;

    // Creating socket file descriptor
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    memset(&cliaddr, 0, sizeof(cliaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = htons(PORT);

    // Fill client information
    cliaddr.sin_family = AF_INET;
    cliaddr.sin_addr.s_addr = INADDR_ANY;
    cliaddr.sin_port = htons(PORT);

    // Bind the socket with the server address
    if (bind(sockfd, (const struct sockaddr *)&servaddr,
             sizeof(servaddr)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    bool scan_descriptor = true;

    while (scan_descriptor)
    {
        int descriptorSize = recvfrom(sockfd, (uint8_t *)headerBuffer, 256, MSG_WAITALL, nullptr, nullptr);
        printf("waiting to get scan descriptor. size %d\n", descriptorSize);

        if (descriptorSize == 7 && headerBuffer[6] == SL_SCAN_DESCRIPTOR_HEADER_FLAG)
        {
            printf("Scan descriptor reveiced ####################################\n");
            scan_descriptor = false;
            break;
        }
    }

    if (cacheScanData())
        printf("Cached data Success");

    /*     while (!viewer.wasStopped())
        {
            if (!cacheScanData())
            {
                printf("Error getting point data");
                return 0;
            }
            printf("Successfull data acquisition");

            signal(SIGINT, ctrlc);

            sl_lidar_response_measurement_node_hq_t *nodes = complete_scan;
            size_t count = _countof(nodes);

            cloud->width = (int)count;
            cloud->height = 1;
            cloud->resize(cloud->width * cloud->height);

            for (int pos = 0; pos < (int)count; ++pos)
            {
                float theta = (nodes[pos].angle_z_q14 * 90.f) / 16384.f;
                float dist = nodes[pos].dist_mm_q2 / 4.0f;

                float x = std::sin((theta * 3.1415) / 180) * dist;
                float y = std::cos((theta * 3.1415) / 180) * dist;

                cloud->points[pos].x = x;
                cloud->points[pos].y = 0;
                cloud->points[pos].z = y;
            }
            viewer.updatePointCloud(cloud, "cloud");
            viewer.spinOnce();

            if (ctrl_c_pressed)comment block
            }
        } */

    return 0;
}