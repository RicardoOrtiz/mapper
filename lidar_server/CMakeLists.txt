cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(stream_from_socket)

find_package(PCL 1.2 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (stream_from_socket stream_from_socket.cpp)
target_link_libraries (stream_from_socket ${PCL_LIBRARIES})