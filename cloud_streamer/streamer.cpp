
/*
Live stream of Lidar data
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <cmath>
#include <fstream>

#include <pcl/visualization/pcl_visualizer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <chrono>
#include <thread>

#include "sl_lidar.h"
#include "sl_lidar_driver.h"
#ifndef _countof
#define _countof(_Array) (int)(sizeof(_Array) / sizeof(_Array[0]))
#endif

#include <unistd.h>

static inline void delay(sl_word_size_t ms)
{
    while (ms >= 1000)
    {
        usleep(1000 * 1000);
        ms -= 1000;
    };
    if (ms != 0)
        usleep(ms * 1000);
}

using namespace sl;
using namespace std;

bool ctrl_c_pressed;
static ILidarDriver *drv = nullptr;

bool checkSLAMTECLIDARHealth(ILidarDriver *drv)
{
    sl_result op_result;
    sl_lidar_response_device_health_t healthinfo;

    op_result = drv->getHealth(healthinfo);
    if (SL_IS_OK(op_result))
    { // the macro IS_OK is the preperred way to judge whether the operation is succeed.
        printf("SLAMTEC Lidar health status : %d\n", healthinfo.status);
        if (healthinfo.status == SL_LIDAR_STATUS_ERROR)
        {
            fprintf(stderr, "Error, slamtec lidar internal error detected. Please reboot the device to retry.\n");
            // enable the following code if you want slamtec lidar to be reboot by software
            // drv->reset();
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        fprintf(stderr, "Error, cannot retrieve the lidar health code: %x\n", op_result);
        return false;
    }
}

void printDeviceInfo(sl_lidar_response_device_info_t devinfo)

{
    // print out the device serial number, firmware and hardware version number..
    printf("SLAMTEC LIDAR S/N: ");
    for (int pos = 0; pos < 16; ++pos)
    {
        printf("%02X", devinfo.serialnum[pos]);
    }

    printf("\n"
           "Firmware Ver: %d.%02d\n"
           "Hardware Rev: %d\n",
           devinfo.firmware_version >> 8, devinfo.firmware_version & 0xFF, (int)devinfo.hardware_version);
}

void ctrlc(int)
{
    ctrl_c_pressed = true;
}

sl_result initializeLidar()
{
    const char *port = NULL;
    sl_u32 baudrate = 1000000ul;
    sl_result op_result;
    int opt_channel_type = CHANNEL_TYPE_SERIALPORT;
    IChannel *_channel;
    port = "/dev/ttyUSB0";
    
    drv = *createLidarDriver();

    if (!drv)
    {
        fprintf(stderr, "insufficent memory, exit\n");
        exit(-2);
    }

    sl_lidar_response_device_info_t devinfo;
    bool connectSuccess = false;

    _channel = (*createSerialPortChannel(port, baudrate));

    if (SL_IS_OK((drv)->connect(_channel)))
    {
        op_result = drv->getDeviceInfo(devinfo);

        if (SL_IS_OK(op_result))
        {
            connectSuccess = true;
        }
        else
        {
            delete drv;
            drv = NULL;
        }
    }

    if (!connectSuccess)
    {
        (fprintf(stderr, "Error, cannot bind to the specified serial port %s.\n", port));
        return 0;
    }

    printDeviceInfo(devinfo);

    if (!checkSLAMTECLIDARHealth(drv))
    {
        return 0;
    }

    signal(SIGINT, ctrlc);

    sl_result res = drv->setMotorSpeed(); // if(opt_channel_type == CHANNEL_TYPE_SERIALPORT)
    
    return res;

}

void stopLidar()
{
    drv->stop();

    delay(200);

    drv->setMotorSpeed(0);
}

int main(int argc, const char *argv[])
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::visualization::PCLVisualizer viewer("PCL Viewer");
    viewer.setBackgroundColor(0.0, 0.0, 0.0);
    viewer.addPointCloud(cloud, "cloud");

    sl_result op_result = initializeLidar();

    if(SL_IS_OK(op_result))
    {
        drv -> startScan(0,1);
    }
    else 
    {
        return 0;
    }

    while (!viewer.wasStopped())
    {
        sl_lidar_response_measurement_node_hq_t nodes[8192];
        size_t count = _countof(nodes);

        cloud->width = (int)count;
        cloud->height = 1;
        cloud->resize(cloud->width * cloud->height);

        op_result = drv->grabScanDataHq(nodes, count);

        if (SL_IS_OK(op_result))
        {
            for (int pos = 0; pos < (int)count; ++pos)
            {
                float theta = (nodes[pos].angle_z_q14 * 90.f) / 16384.f;
                float dist = nodes[pos].dist_mm_q2 / 4.0f;

                float x = std::sin((theta * 3.1415) / 180) * dist;
                float y = std::cos((theta * 3.1415) / 180) * dist;

                cloud->points[pos].x = x;
                cloud->points[pos].y = 0;
                cloud->points[pos].z = y;
            }
            viewer.updatePointCloud(cloud, "cloud");
            viewer.spinOnce();
        }

        if (ctrl_c_pressed)
        {
            break;
        }
    }

    stopLidar();

    return 0;
}
